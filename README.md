# OpenML dataset: andro

https://www.openml.org/d/41392

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multivariate regression data set from: https://link.springer.com/article/10.1007%2Fs10994-016-5546-z : The Andromeda dataset (Hatzikos et al. 2008) concerns the prediction of future values for six water quality variables (temperature, pH, conductivity, salinity, oxygen, turbidity) in Thermaikos Gulf of Thessaloniki, Greece. Measurements of the target variables are taken from under-water sensors with a sampling interval of 9 seconds and then averaged to get a single measurement for each variable over each day. The specific dataset that we use here corresponds to using a window of 5 days (i.e. features attributes correspond to the values of the six water quality variables up to 5 days in the past) and a lead of 5 days (i.e. we predict the values of each variable 6 days ahead).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41392) of an [OpenML dataset](https://www.openml.org/d/41392). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41392/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41392/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41392/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

